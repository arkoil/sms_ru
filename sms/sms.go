package sms

type SMS interface {
	Phone() string
	Message() string
}
