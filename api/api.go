// package for sms.ru API
package api

import (
	"gitlab.com/arkoil/sms_ru/sms"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

// struct for APi data
type APIHandler struct {
	apiID   string
	baseUrl string
	Client  *http.Client
	Test    bool
}

type APIError struct {
	Err       string
	ErrCode   string
	ReqStatus string
}

func (err APIError) Error() string {
	return err.Err
}

func NewAPIHandler(apiID string, client *http.Client, test bool) *APIHandler {
	return &APIHandler{apiID: apiID, baseUrl: "https://sms.ru", Client: client, Test: test}
}
func (api *APIHandler) setBaseURL(baseUrl string) {
	api.baseUrl = baseUrl
}

func (api APIHandler) SendSMSList(smsList *[]sms.SMS, json bool) (string, error) {
	var err error
	err = APIError{}
	clearRes := ""
	if len(*smsList) == 0 {
		apiError := APIError{"sms list is empty", "001", ""} //errors.New("")
		return clearRes, apiError
	}
	smsFromData := url.Values{}
	for _, msg := range *smsList {
		key := "to[" + msg.Phone() + "]"
		smsFromData.Add(key, msg.Message())
	}
	req, err := http.NewRequest("POST", api.baseUrl, strings.NewReader(smsFromData.Encode()))
	if err != nil {
		return clearRes, err
	}
	req.Header.Set("content-type", "application/x-www-form-urlencoded")
	q := req.URL.Query()
	q.Add("api_id", api.apiID)
	if json {
		q.Add("json", "1")
	}
	if api.Test {
		q.Add("test", "1")
	}
	req.Header.Set("content-type", "application/x-www-form-urlencoded")
	req.URL.RawQuery = q.Encode()
	resp, err := api.Client.Do(req)
	if err != nil {
		return clearRes, err
	}
	aoiAnswer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return clearRes, err
	}
	return string(aoiAnswer), nil
}
