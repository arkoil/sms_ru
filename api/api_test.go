package api

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/arkoil/sms_ru/sms"
)

const (
	testAPIID = "TEST_API_ID"
)

type message struct {
}

func (m message) Phone() string {
	return "+79997779977"
}
func (m message) Message() string {
	return "TextTextText"
}
func TestAPIHandler_SendSMSList(t *testing.T) {
	client := &http.Client{}
	api := NewAPIHandler("TEST_API_ID", client, true)
	t.Run("Send sms empty list", func(t *testing.T) {
		list := make([]sms.SMS, 0)
		//list = append(list, message{})
		_, got := api.SendSMSList(&list, true)
		if got == nil {
			t.Error("Not empty error message")
		}
	})
	t.Run("Send sms", func(t *testing.T) {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			res := "{}"
			params := r.URL.Query()
			apiId := params.Get("api_id")
			fmt.Println(apiId)
			err := r.ParseForm()
			if err != nil {
				t.Error(err)
			}
			if apiId == testAPIID {
				res = "{\"status\": \"OK\",\"status_code\": 100\"sms\": {\"79067925823\": {\"status\": \"OK\", \"status_code\": 100, \"sms_id\": \"000000-10000000\" },\"74993221627\": {\"status\": \"ERROR\",\"status_code\": 207, \"status_text\": \"На этот номер (или один из номеров) нельзя отправлять сообщения, либо указано более 100 номеров в списке получателей\" }} ,\"balance\": 4122.56 }"
			}
			_, err = w.Write([]byte(res))
			if err != nil {
				t.Error(err)
			}
		}))
		defer server.Close()
		api.setBaseURL(server.URL)
		list := make([]sms.SMS, 0)
		list = append(list, message{})
		list = append(list, message{})
		list = append(list, message{})
		list = append(list, message{})
		res, err := api.SendSMSList(&list, true)
		if err != nil {
			t.Error(err)
		}
		wait := "{\"status\": \"OK\",\"status_code\": 100\"sms\": {\"79067925823\": {\"status\": \"OK\", \"status_code\": 100, \"sms_id\": \"000000-10000000\" },\"74993221627\": {\"status\": \"ERROR\",\"status_code\": 207, \"status_text\": \"На этот номер (или один из номеров) нельзя отправлять сообщения, либо указано более 100 номеров в списке получателей\" }} ,\"balance\": 4122.56 }"
		if res != wait {
			t.Errorf("Invalid status: got %v, wait %v", res, wait)
		}
	})
}
